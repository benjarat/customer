FROM alpine     

RUN apk update       
RUN apk add --no-cache nodejs-current nodejs-npm

RUN mkdir -p /usr/src 
WORKDIR /usr/src                       

COPY . .

RUN npm install

EXPOSE 3000

ENTRYPOINT ["/bin/sh","-c"]

CMD ["npm start"] 